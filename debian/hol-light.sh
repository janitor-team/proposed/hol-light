#!/bin/sh

#set -xe

if [ -f /usr/bin/readline-editor ] ; then
    readline=/usr/bin/readline-editor
else
    readline=
fi

exec $readline /usr/bin/ocaml $* -init /usr/share/hol-light/hol.ml
